import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()
	

setup(name='xkeys',
      version='1.0',
      description='XKeys API Python binding',
	  long_description=read('README.txt'),
      author='Brent Yates',
      author_email='brent.yates@gmail.com',
	  url="https://bitbucket.org/byates/xkeys_python_binding",
	  py_modules = ['xkeys_api', 'xkeys_device'],
	  data_files=[('.',['PIEHid.dll'])],
	  classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Environment :: Win32 (MS Windows)',
        'Intended Audience :: Developers',
        'License :: GNU Lesser General Public License v2 or later (LGPLv2+)',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python'
        ]
     )