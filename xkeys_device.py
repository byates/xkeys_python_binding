# GUI XKeys Python Binding
# Copyright (C) 2012 Brent Yates
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the
#    Free Software Foundation, Inc.,
#    59 Temple Place,
#    Suite 330,
#    Boston, MA 02111-1307 USA

#=================================================================================
# This class represents a single XKeys device and uses the XKeys API to query and
# control the device.
#=================================================================================

from __future__ import print_function
import sys
import time
import binascii
from ctypes import *
from ctypes.wintypes import *
from xkeys_api import *

class XkeysData(object):
    '''
    This class holds the data returned via event callbacks
    '''

    def __init__(self, keys = None, deviceID = None, unitID = None, timestamp = None, programSwitch = ""):
        self.keys = keys
        self.deviceID = deviceID
        self.unitID = unitID
        self.timestamp = timestamp
        self.programSwitch = programSwitch


class XkeysLed(object):
    '''
    This class represents a single LED and is used when building the LED output message.
    '''

    def __init__(self, color, state, index = 0):
        '''
        color is either 'green' or 'red'
        index is from 0 to number of LED's of that color
        state is either 'on', 'flash', or 'off'
        '''
        self.color = color
        self.index = index
        self.state = state


class XkeysDevice(object):
    '''
    This class represents a single XKeys device.
    '''

    # Method signature for use with the API callback routines
    _callback_t = WINFUNCTYPE(c_long, POINTER(c_ubyte), c_long, c_long)


    def __init__(self, xkeys_api, pid, hid_usage, hid_usage_page, handle):
        '''
        Initialize a device instance.  The parameters are usually from a call to the
        EnumeratePIE() function.
        '''
        self.xkeys_api = xkeys_api
        self.PID = pid
        self.hid_usage = hid_usage
        self.hid_usage_page = hid_usage_page
        self.handle = handle
        self.callback = None

        self._PIDSpecificInitialization()

        #=======================================================================
        # Keys in this list are toggle on/off meaning that they will be added to
        # the active keys list on one press and removed on another.
        #=======================================================================

        self.ToggleKeysList = []
        # This is the list of 'ToggleKeys' that are active.
        self._ToggleKeys_ActiveList = []

    def _PIDSpecificInitialization(self):
        '''
        Each keyboard type has differing parameters that we must know in order to
        parse data coming in and to properly format data going out.  Set those here.

        NOTE: All indexes are set to match those in the PI Engineering doc which are '1'
              based.
        '''

        # Defaults
        self.Name = "Unknown"
        self.ReportLength = 0
        self.UnitIDIndex = 0
        self.KeysRange = None
        self.TimestampIndex = 0
        self.ProgSwIndex = 0
        self.NumberOfRedLEDs = 0
        self.NumberOfGreenLEDs = 0
        self.HasBlacklights = False

        # Professional
        if self.PID in [0x02A7]:
            self.Name = "Professional"
            self.ReportLength = 32
            self.UnitIDIndex = 11
            self.KeysRange = range(2, 11)
            self.TimestampIndex = 0
            self.ProgSwIndex = 12
            self.NumberOfRedLEDs = 1
            self.NumberOfGreenLEDs = 1
            self.HasBlacklights = False

        # XK-80/60
        if self.PID in [0x0441, 0x0443, 0x461, 0x0463]:
            self.Name = "XK-80/60"
            self.ReportLength = 33
            self.UnitIDIndex = 2
            self.KeysRange = range(4, 14)
            self.TimestampIndex = 14
            self.ProgSwIndex = 3
            self.NumberOfRedLEDs = 1
            self.NumberOfGreenLEDs = 1
            self.HasBlacklights = True


    def SetupInterface(self, suppressDuplicateReports = True):
        '''
        This call opens handles for reading from and writing to the device, and creates
        a dedicated read thread to constantly watch the device for new input. Therefore,
        SetupInterface() must be called before any read or write calls are made.
        To receive the duplicate reports set suppressDuplicateReports to FALSE. In general,
        it is best only to call this function once, and leave the read and write handles open.
        If you must repeatedly call this function, make sure that your code calls CloseInterface()
        every time, before SetupInferface() is called again.
        '''
        result = self.xkeys_api.SetupInterface(self.handle, suppressDuplicateReports)
        return(result)

    def CloseInterface(self):
        '''
        Call this function when you are done using an open device, and are unlikely to use it
        again. It sets a flag telling the read thread to exit, closes the read handle, forcing
        the current outstanding read to abort, and waits up to 100 mSec for this to occur. Then
        it halts the write queue, deletes read and write buffers, and releases system resources
        (closes mutexes and events). This function should be called before calling
        SetupInterface() again for this particular interface.
        '''
        result = self.xkeys_api.CloseInterface(self.handle)
        return(result)

    def ReadData(self):
        '''
        returns a byte_array containing any data collected

        Copies the first HID read report present in the ring buffer into data.
        If this function call returns a data packet, then the packet is removed from the front
        of the ring buffer, so that the next call will return the next packet.
        '''
        (Result, Data) = self.xkeys_api.ReadData(self.handle)
        if Result == 304:
            return(None)
        Result = bytearray()
        for index in range(33):
            Result.append(Data[index])
        return(Result)

    def BlockingReadData(self, maxMillis):
        '''
        @maxMillis: number of milliseconds to wait for a data block
        returns a byte_array containing any data collected

        If there is no queued data available, this call will wait up to maxMillis milliseconds
        for data to arrive. An error status of 304 is returned if there is no data appearing
        in the buffer within the time specified.
        Using this call improves the process of waiting to get a specific report back after,
        for example, sending a Check Dongle output report. An error status of 307 means the
        handle is invalid or the device has been disconnected.
        If this function call returns a data packet, then the packet is removed from the front
        of the ring buffer, so that the next call will return the next packet.
        '''
        (Result, Data) = self.xkeys_api.BlockingReadData(self.handle, maxMillis)
        if Result == 304:
            return(None)
        Result = bytearray()
        for index in range(33):
            Result.append(Data[index])
        return(Result)

    def ClearBuffer(self):
        '''
        This function deletes all remaining data packets from the read buffer.
        '''
        result = self.xkeys_api.ClearBuffer(self.handle)
        return(result)

    def GetReadLength(self):
        '''
        Returns the number of bytes in the input report.
        '''
        result = self.xkeys_api.GetReadLength(self.handle)
        if result == 0xFFFFFFFF:
            print("Invalid handle")
            result = 0
        return(result)


    def GetWriteLength(self):
        '''
        Returns the number of bytes in an output report.
        '''
        result = self.xkeys_api.GetWriteLength(self.handle)
        if result == 0xFFFFFFFF:
            print("Invalid handle")
            result = 0
        return(result)


    def WriteData(self, data):
        result = self.xkeys_api.WriteData(self.handle, data)
        return(result)


    def SetCallback(self, callback):
        '''
        Call this function to receive an event notification for an application when data
        from the device has changed.
        If callback is None then callbacks are disabled.
        Callback signature: cb(XkeysDevice, XkeysData)
        '''
        def ClosureCallback(pData, deviceID, error):
            '''
            @pData: POINTER(c_ubyte)
            @deviceID: c_long
            @error: c_long

            This is the function actually called by the API DLL.  It is here so that it acts
            as a closure.  self is valid inside this function.
            See http://www.python.org/dev/peps/pep-3104/ for more details on closures.
            '''
            # print("ClosureCallback called. deviceID={0}, error={1}".format(deviceID, error))
            if self._CallbackTarget:
                MsgData = bytearray()
                for index in range(self.IncomingMsgLength):
                    MsgData.append(pData[index])
                if self.isKeyDataMsg(MsgData):
                    KeyList = self.ConvertKeyDataToList(MsgData)
                    KeyList = self._ResolveToggleKeys(KeyList)
                    UnitID = self.ExtractUnitID(MsgData)
                    Timestamp = self.ExtractTimestamp(MsgData)
                    ProgramSwitch = self.ExtractProgramSwitchState(MsgData)
                    data = XkeysData(keys = KeyList,
                                      deviceID = deviceID,
                                      unitID = UnitID,
                                      timestamp = Timestamp,
                                      programSwitch = ProgramSwitch)
                    self._CallbackTarget(self, data)
            return(True)

        # We have to save the reference to the closure otherwise it is garbage collected
        # and we crash.  We use self.xxx to make it last as long as the class instance.
        self._closure_callback = self._callback_t(ClosureCallback)

        # We need to know how long the message we will get is
        self.IncomingMsgLength = self.GetReadLength()

        if callback:
            self._CallbackTarget = callback
            result = self.xkeys_api.SetDataCallback(self.handle, self._closure_callback)
        else:
            self._CallbackTarget = None
            result = self.xkeys_api.SetDataCallback(self.handle, None)
        return(result)


    def isKeyDataMsg(self, rawByteArray):
        '''
        Determine if this message contains key data or some other data.
        '''
        Result = False

        # Professional
        if (self.PID == 0x02A7):
            Result = (rawByteArray[0] == 2)

        # XK-80/60
        if self.PID in [0x0441, 0x0443, 0x461, 0x0463]:
            Result = (rawByteArray[3] < 10)

        return(Result)


    def ConvertKeyDataToList(self, rawByteArray):
        Result = []
        if not self.KeysRange:
            return(Result)
        for index in self.KeysRange:
            byte = rawByteArray[index - 1]
            for bit in range(8):
                if byte & (2 ** bit) != 0:
                    Result.append((index - self.KeysRange[0], bit))
        return(Result)


    def _ResolveToggleKeys(self, KeyList):
        # If a key is in the KeyList and in the ToggleKeys list then toggle the key
        # in the ToggleKeys active list and remove it from the KeyList.
        # When all the keys have been processed, then append the ToggleKeys active
        # list to the keys list.
        for key in KeyList:
            if key in self.ToggleKeysList:
                if key in self._ToggleKeys_ActiveList:
                    self._ToggleKeys_ActiveList.remove(key)
                else:
                    self._ToggleKeys_ActiveList.append(key)
                KeyList.remove(key)
        KeyList.extend(self._ToggleKeys_ActiveList)
        return(KeyList)

    def ExtractUnitID(self, rawByteArray):
        UnitID = 0
        if self.UnitIDIndex == 0:
            return(UnitID)
        UnitID = rawByteArray[self.UnitIDIndex - 1]
        return(UnitID)

    def ExtractTimestamp(self, rawByteArray):
        Timestamp = 0
        if self.TimestampIndex == 0:
            return(Timestamp)
        Timestamp = ((rawByteArray[self.TimestampIndex - 1 + 3] << 0) +
                     (rawByteArray[self.TimestampIndex - 1 + 2] << 8) +
                     (rawByteArray[self.TimestampIndex - 1 + 1] << 16) +
                     (rawByteArray[self.TimestampIndex - 1 + 0] << 24))
        return(Timestamp)


    def ExtractProgramSwitchState(self, rawByteArray):
        ProgramSwitch = ""
        if self.ProgSwIndex == 0:
            return(ProgramSwitch)

        # Professional
        if (self.PID == 0x02A7):
            if (rawByteArray[self.ProgSwIndex - 1] & 0x10) == 0x10:
                ProgramSwitch = "ON"
            else:
                ProgramSwitch = "OFF"

        # XK-80/60
        if self.PID in [0x0441, 0x0443, 0x461, 0x0463]:
            PS = rawByteArray[self.ProgSwIndex - 1]
            if (PS == 0) or (PS == 2):
                ProgramSwitch = "OFF"
            else:
                ProgramSwitch = "ON"

        return(ProgramSwitch)


    def SetLeds(self, led_list):
        '''Sets the device's LED's based on the values in the LED list.

        The led_list should hold XkeysLed objects
        '''
        output = self._BuildOutputMessage_SetLEDs(led_list)
        for msg in output:
            Result = self.WriteData(msg)
            if Result != 0:
                print("WriteData failed with error code = {0}".format(Result))
                break


    def SetUnitID(self, unitID):
        '''Sets the device's Unit ID.

        The unitID should be a byte
        '''
        msg = self._BuildOutputMessage_SetUnitID(unitID)
        Result = self.WriteData(msg)
        if Result != 0:
            print("WriteData failed with error code = {0}".format(Result))


    def SetBacklight(self, location, state, color):
        '''
        Sets a specific backlight location.

        location = (row, col)
        state = 'on', 'off', 'flash'
        color = 'red' or 'blue'
        '''
        bank = 0
        if color == 'red':
            bank = 1
        index = (location[0] * 8 + location[1]) + bank * 80
        msg = self._BuildOutputMessage_SetBacklight(index, state)
        if len(msg) > 0:
            Result = self.WriteData(msg)
            if Result != 0:
                print("WriteData failed with error code = {0}".format(Result))


    def SetBacklightIntensity(self, red_intensity, blue_intensity):
        '''
        Sets the backlight intensity (0 = off, 255 = max).

        '''
        msg = self._BuildOutputMessage_SetBacklightIntensity(red_intensity, blue_intensity)
        if len(msg) > 0:
            Result = self.WriteData(msg)
            if Result != 0:
                print("WriteData failed with error code = {0}".format(Result))


    def SetBacklightAllOff(self, color):
        '''
        Turns off all the backlights for a given color

        '''
        msg = self._BuildOutputMessage_SetBacklightRows(color, 0)
        if len(msg) > 0:
            Result = self.WriteData(msg)
            if Result != 0:
                print("WriteData failed with error code = {0}".format(Result))


    def _BuildOutputMessage_SetLEDs(self, led_list):
        '''
        Assembles a byte array containing a message to be sent to the keyboard
        which will enable/disable certain LEDs.

        The input list should contain XkeysLed() objects.

        The output is a list of bytearrays, each one containing a message that should
        be sent to effect the LED changes.
        '''
        OutputList = []

        # Professional
        if (self.PID == 0x02A7):
            OutputMsg = bytearray()
            OutputMsg.append(2)
            OutputMsg.append(186)
            for _ in range(5):
                OutputMsg.append(0)
            LedControlByte = 0
            for led in led_list:
                if (led.index == 0):
                    if led.color == 'red':
                        if (led.state == 'off'):
                            LedControlByte = LedControlByte & ~0x80
                        else:
                            LedControlByte = LedControlByte | 0x80
                    if led.color == 'green':
                        if (led.state == 'off'):
                            LedControlByte = LedControlByte & ~0x40
                        else:
                            LedControlByte = LedControlByte | 0x40
            OutputMsg.append(LedControlByte)
            OutputList.append(OutputMsg)

        # XK-80/60
        if self.PID in [0x0441, 0x0443, 0x461, 0x0463]:
            for led in led_list:
                if (led.index == 0):
                    OutputMsg = bytearray()
                    OutputMsg.append(0)
                    OutputMsg.append(179)
                    if led.color == 'red':
                        OutputMsg.append(7)
                    else:
                        OutputMsg.append(6)
                    if (led.state == 'flash'):
                        OutputMsg.append(2)
                    elif (led.state == 'on'):
                        OutputMsg.append(1)
                    else:
                        OutputMsg.append(0)
                    for _ in range(5, 37):
                        OutputMsg.append(0)
                    OutputList.append(OutputMsg)

        return(OutputList)


    def _BuildOutputMessage_SetUnitID(self, unitID):
        '''
        Assembles a byte array containing a message to be sent to the keyboard
        which will set the unit ID of the keyboard.
        '''
        OutputMsg = bytearray()

        # Professional
        if (self.PID == 0x02A7):
            OutputMsg.append(2)
            OutputMsg.append(189)
            OutputMsg.append(unitID)
            for _ in range(4, 9):
                OutputMsg.append(0)

        # XK-80/60
        if self.PID in [0x0441, 0x0443, 0x461, 0x0463]:
            OutputMsg.append(0)
            OutputMsg.append(189)
            OutputMsg.append(unitID)
            for _ in range(4, 37):
                OutputMsg.append(0)

        return(OutputMsg)


    def _BuildOutputMessage_SetBacklight(self, index, state):
        '''
        Assembles a byte array containing a message to be sent to the keyboard
        which will set the backlight of an individual location.

        For bank 1 the index is 0-79, where 0-7 is 1st column, 8-15 is 2nd column, etc.
        For bank 2 add 80 to the bank 1 indices. For example to control the lowest left
        key bank 1 index=7, the corresponding bank 2 is index=87.

        state = 'off', 'on', or 'flash'
        '''
        OutputMsg = bytearray()

        # XK-80/60
        if self.PID in [0x0441, 0x0443, 0x461, 0x0463]:
            OutputMsg.append(0)
            OutputMsg.append(181)
            OutputMsg.append(index)
            if (state == 'flash'):
                OutputMsg.append(2)
            elif (state == 'on'):
                OutputMsg.append(1)
            else:
                OutputMsg.append(0)
            for _ in range(5, 37):
                OutputMsg.append(0)

        return(OutputMsg)


    def _BuildOutputMessage_SetBacklightIntensity(self, red_intensity, blue_intensity):
        '''
        Assembles a byte array containing a message to be sent to the keyboard
        which will set the backlight intensity for each color/bank

        red_intensity = 0 255
        blue_intensity = 0 255
        '''
        OutputMsg = bytearray()

        # XK-80/60
        if self.PID in [0x0441, 0x0443, 0x461, 0x0463]:
            OutputMsg.append(0)
            OutputMsg.append(187)
            OutputMsg.append(blue_intensity)
            OutputMsg.append(red_intensity)
            for _ in range(5, 37):
                OutputMsg.append(0)

        return(OutputMsg)


    def _BuildOutputMessage_SetBacklightRows(self, color, rows):
        '''
        Assembles a byte array containing a message to be sent to the keyboard
        which will set row for each color/bank

        color = red or blue
        rows = byte with each bit corresponding to a row
        '''
        OutputMsg = bytearray()

        # XK-80/60
        if self.PID in [0x0441, 0x0443, 0x461, 0x0463]:
            OutputMsg.append(0)
            OutputMsg.append(182)
            if color == 'red':
                OutputMsg.append(1)
            else:
                OutputMsg.append(0)
            OutputMsg.append(rows)
            for _ in range(5, 37):
                OutputMsg.append(0)

        return(OutputMsg)



