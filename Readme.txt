xkeys.py 
(c) Brent Yates 2012
Released under the LGPL licence


What is it
----------
xkeys.py is a Python binding for the xkeys family of keyboards.  See http://www.piengineering.com.


Installation
------------

easy_install dist\xkeys-1.0-py2.7.egg

To Build the installer EGG (not necessary unless the source changes):
python setup.py bdist_egg

Where to start
--------------
Look at the examples provided under Examples directory.



