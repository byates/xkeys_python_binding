import sys
import os
from PyQt4 import QtCore, QtGui
from MainForm import Ui_MainWindow

try:
    # First try to get the xkeys modules from the pyhton path.  This will work if we have
    # been installed.
    from xkeys_api import *
    from xkeys_device import *
except:
    # If not installed then look for the modules in the default repository directory layout
    sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
    from xkeys_api import *
    from xkeys_device import *

class SampleApp(QtCore.QObject):

    def __init__(self):
        QtCore.QObject.__init__(self)
        self.QtApp = QtGui.QApplication(sys.argv)
        self.QtMain = QtGui.QMainWindow()
        self.QtUiMain = Ui_MainWindow()
        # Modify the main window with UI elements from QtDesigner
        self.QtUiMain.setupUi(self.QtMain)
        # Setup an XKeys link
        self.SetupXKeysLink()
        # Modify the main with custom stuff like signal connections
        self.SetupQtSignalConnections()

    XKeysSignal = QtCore.pyqtSignal(xkeys_device.XkeysDevice, xkeys_device.XkeysData)

    def ShowMain(self):
        self.QtMain.show()


    def Exec_(self):
        self.QtApp.exec_()


    def SetupXKeysLink(self):
        # Setup the XKeys link
        self.XKeys = XkeysAPI()
        self.XkeysDeviceList = self.XKeys.EnumeratePIE()
        if len(self.XkeysDeviceList) == 0:
            self.QtUiMain.textBrowser.append("No XKeys devices found!")
        else:
            for index, Device in enumerate(self.XkeysDeviceList):
                self.QtUiMain.textBrowser.append("Device[{0}]=PID:{1}".format(index, Device.PID))
                self.QtUiMain.textBrowser.append("Attaching to XKeys device[{0}]".format(index))
                Device = self.XkeysDeviceList[index]
                Result = Device.SetupInterface()
                if Result == 0:
                    self.QtUiMain.textBrowser.append("    SetupInterface = {0}".format(Result))
                    Result = Device.SetCallback(self.XKeysCallback)
                    # self.QtUiMain.textBrowser.append("    SetCallback = {0}".format(Result))
            self.XKeysSignal.connect(self.OnXKeysEvent)


    def SetupQtSignalConnections(self):
        self.QtUiMain.checkBox.stateChanged.connect(self.OnCheckboxStateChange)


    def OnCheckboxStateChange(self, state):
        if state:
            if not (0, 1) in self.XkeysDeviceList[0].ToggleKeysList:
                self.XkeysDeviceList[0].ToggleKeysList.append((0, 0))
        else:
            self.XkeysDeviceList[0].ToggleKeysList.remove((0, 0))

    def XKeysCallback(self, XkeysDevice, XkeysData):
        '''This callback is executed in the XKeys API thread.  It is not safe to call much
        here due to threading issues.  It is safe to emit a QT signal.
        '''
        self.XKeysSignal.emit(XkeysDevice, XkeysData)

    def OnXKeysEvent(self, XkeysDevice, XkeysData):
        '''Called each time an XKeys signal is received.  The thread context is in the main
        application loop so everything should work here.
        '''
        self.QtUiMain.textBrowser.append("XKeysCallback: PID={0} device_id={1} handle={2}".
                                         format(XkeysDevice.PID, XkeysData.deviceID, XkeysDevice.handle))
        line = ""
        self.QtUiMain.textBrowser.append("    " + str(XkeysData.keys))
        self.QtUiMain.textBrowser.append("    " + "UnitID=" + str(XkeysData.unitID))
        self.QtUiMain.textBrowser.append("    " + "Timestamp=" + str(XkeysData.timestamp))
        self.QtUiMain.textBrowser.append("    " + "ProgramSwitch=" + str(XkeysData.programSwitch))


def main():

    app = SampleApp()
    app.ShowMain()
    sys.exit(app.Exec_())


if __name__ == '__main__':
    main()
