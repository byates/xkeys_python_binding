import sys
import os
try:
    # First try to get the xkeys modules from the pyhton path.  This will work if we have
    # been installed.
    from xkeys_api import *
    from xkeys_device import *
except:
    # If not installed then look for the modules in the default repository directory layout
    sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
    from xkeys_api import *
    from xkeys_device import *

class Main(object):

    def __init__(self):
        self.XKeys = XkeysAPI()
        self.DeviceList = self.XKeys.EnumeratePIE()
        print("Device Count={0}".format(len(self.DeviceList)))
        if len(self.DeviceList) == 0:
            exit
        for index, Device in enumerate(self.DeviceList):
            assert isinstance(Device, xkeys_device.XkeysDevice)
            print("Device[{0}]=PID:{1}  '{2}'".format(index, Device.PID, Device.Name))


    def UserCallback(self, xkeysDevice, xkeysData):
        print("UserCallback: PID={0} device_id={1} unit_id={2}".format(xkeysDevice.PID,
                                                                       xkeysData.deviceID,
                                                                       xkeysData.unitID));
        print(xkeysData.keys)

    def Run(self):
        Device = self.DeviceList[0]
        Result = Device.SetupInterface()
        print("SetupInterface = {0}".format(Result))
        Result = Device.SetCallback(self.UserCallback)
        print("SetCallback = {0}".format(Result))

        if Device.HasBlacklights:
            Device.SetBacklightAllOff('blue')
            Device.SetBacklightAllOff('red')
            Device.SetBacklight((0, 0), 'off', 'blue')
            Device.SetBacklight((1, 1), 'on', 'red')
            Device.SetBacklight((2, 2), 'off', 'red')
            Device.SetBacklightIntensity(255, 255)

        Device.SetLeds([XkeysLed("green", "on"), XkeysLed("red", "on")])
        choice = raw_input("Press Enter to exit:>\n")
        Device.SetLeds([XkeysLed("green", "off"), XkeysLed("red", "off")])
        Device.CloseInterface()
        print("Closed")


if __name__ == "__main__":
    m = Main()
    if len(m.DeviceList):
        m.Run()


