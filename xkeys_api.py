# GUI XKeys Python Binding
# Copyright (C) 2012 Brent Yates
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the
#    Free Software Foundation, Inc.,
#    59 Temple Place,
#    Suite 330,
#    Boston, MA 02111-1307 USA

#=================================================================================
# Class which wraps the P.I. Engineering Software Development Kit using ctypes.
#=================================================================================

from __future__ import print_function
import sys
import os
import time
import binascii
from ctypes import *
from ctypes.wintypes import *
import xkeys_device

class XkeysAPI(object):
    '''
    Class which wraps the P.I. Engineering Software Development Kit using ctypes.
    '''

    _callback_t = WINFUNCTYPE(c_long, POINTER(c_ubyte), c_long, c_long)

    def __init__(self):
        '''
        Initializes the class by defining all of the API methods into a dictionary.
        '''

        # Assume DLL is in the same directory as this file.
        self.xkeysdll = windll.LoadLibrary(os.path.join(os.path.dirname(__file__), 'PIEHid.dll'))

        self.apiEnumeratePIE = self.xkeysdll.EnumeratePIE
        self.apiEnumeratePIE.restype = c_long
        self.apiEnumeratePIE.argtypes = [c_long, POINTER(c_long), POINTER(c_long)]

        self.apiSetupInterface = self.xkeysdll.SetupInterfaceEx
        self.apiSetupInterface.restype = c_long
        self.apiSetupInterface.argtypes = [c_long, BOOL]

        self.apiCloseInterface = self.xkeysdll.CloseInterface
        self.apiCloseInterface.restype = c_long
        self.apiCloseInterface.argtypes = [c_long]

        self.apiReadData = self.xkeysdll.ReadData
        self.apiReadData.restype = c_long
        self.apiReadData.argtypes = [c_long, POINTER(c_ubyte)]

        self.apiBlockingReadData = self.xkeysdll.BlockingReadData
        self.apiBlockingReadData.restype = c_long
        self.apiBlockingReadData.argtypes = [c_long, POINTER(c_ubyte), c_int]

        self.apiClearBuffer = self.xkeysdll.ClearBuffer
        self.apiClearBuffer.restype = c_long
        self.apiClearBuffer.argtypes = [c_long]

        self.apiGetReadLength = self.xkeysdll.GetReadLength
        self.apiGetReadLength.restype = c_long
        self.apiGetReadLength.argtypes = [c_long]

        self.apiGetWriteLength = self.xkeysdll.GetWriteLength
        self.apiGetWriteLength.restype = c_long
        self.apiGetWriteLength.argtypes = [c_long]

        self.apiSetDataCallback = self.xkeysdll.SetDataCallbackEx
        self.apiSetDataCallback.restype = c_long
        self.apiSetDataCallback.argtypes = [c_long, c_int, self._callback_t, c_bool]

        self.apiWriteData = self.xkeysdll.WriteData
        self.apiWriteData.restype = c_long
        self.apiWriteData.argtypes = [c_long, POINTER(c_ubyte)]



    def EnumeratePIE(self):
        '''
        Call EnumeratePIE() once, before doing anything else, to determine a list of
        available devices.
        Returns a list of available devices of type xkeys_device.
        '''
        DeviceData = (c_long * 256)()
        Count = c_long()
        ErrorCode = self.apiEnumeratePIE(c_long(1523), DeviceData, byref(Count))
        DeviceList = []
        if ErrorCode == 0:
            for index in range(Count.value):
                Device = xkeys_device.XkeysDevice(self,
                                      DeviceData[index * 4 + 0],
                                      DeviceData[index * 4 + 1],
                                      DeviceData[index * 4 + 2],
                                      DeviceData[index * 4 + 3])
                DeviceList.append(Device)
        return(DeviceList)

    def SetupInterface(self, handle, suppressDuplicateReports = True):
        '''
        This call opens handles for reading from and writing to the device, and creates
        a dedicated read thread to constantly watch the device for new input. Therefore,
        SetupInterface() must be called before any read or write calls are made.
        To receive the duplicate reports set suppressDuplicateReports to FALSE. In general,
        it is best only to call this function once, and leave the read and write handles open.
        If you must repeatedly call this function, make sure that your code calls CloseInterface()
        every time, before SetupInferface() is called again.
        '''
        result = self.apiSetupInterface(handle, suppressDuplicateReports)
        return(result)

    def CloseInterface(self, handle):
        '''
        Call this function when you are done using an open device, and are unlikely to use it
        again. It sets a flag telling the read thread to exit, closes the read handle, forcing
        the current outstanding read to abort, and waits up to 100 mSec for this to occur. Then
        it halts the write queue, deletes read and write buffers, and releases system resources
        (closes mutexes and events). This function should be called before calling
        SetupInterface() again for this particular interface.
        '''
        result = self.apiCloseInterface(handle)
        return(result)

    def ReadData(self, handle):
        '''
        Copies the first HID read report present in the ring buffer into data.
        If this function call returns a data packet, then the packet is removed from the front
        of the ring buffer, so that the next call will return the next packet.
        '''
        Data = (c_ubyte * 256)()
        result = self.apiReadData(handle, Data)
        return((result, Data))


    def BlockingReadData(self, handle, maxMillis):
        '''
        @handle: handle to a device discovered via Enumerate
        @maxMillis: number of milliseconds to wait for a data block
        returns an (result_code, Data[256])

        If there is no queued data available, this call will wait up to maxMillis milliseconds
        for data to arrive. An error status of 304 is returned if there is no data appearing
        in the buffer within the time specified.
        Using this call improves the process of waiting to get a specific report back after,
        for example, sending a Check Dongle output report. An error status of 307 means the
        handle is invalid or the device has been disconnected.
        If this function call returns a data packet, then the packet is removed from the front
        of the ring buffer, so that the next call will return the next packet.
        '''
        Data = (c_ubyte * 256)()
        result = self.apiBlockingReadData(handle, Data, maxMillis)
        return((result, Data))

    def ClearBuffer(self, handle):
        '''
        This function deletes all remaining data packets from the read buffer.
        '''
        result = self.apiClearBuffer(handle)
        return(result)

    def GetReadLength(self, handle):
        '''
        Returns the number of bytes in the input report.
        A return value of -1 (hexadecimal 0xFFFF) indicates that an invalid handle
        value was passed.
        '''
        result = self.apiGetReadLength(handle)
        return(result)

    def GetWriteLength(self, handle):
        '''
        Returns the number of bytes in an output report.
        A return value of -1 (hexadecimal 0xFFFF) indicates that an invalid handle
        value was passed.
        '''
        result = self.apiGetWriteLength(handle)
        return(result)

    def SetDataCallback(self, handle, callback_target):
        '''
        Call this function to receive an event notification for an application when data
        from the device has changed.
        callback_target must have the signature of:
             @pData: POINTER(c_ubyte)
             @deviceID: c_long
             @error: c_long
        '''

        if callback_target:
            result = self.apiSetDataCallback(handle, 1, callback_target, False)
        else:
            result = self.apiSetDataCallback(handle, 0, callback_target, False)

        return(result)

    def WriteData(self, handle, data):
        '''
        Writes the data to the device specified by handle.

        Data should be a bytearray containing a properly formated output message.
        '''
        ExpectedOutputLength = self.GetWriteLength(handle)
        ApiData = (c_ubyte * ExpectedOutputLength)()
        if len(data) != ExpectedOutputLength:
            raise BufferError("WriteData data length is not correct for the Keyboard specified.")
        for index in range(ExpectedOutputLength):
            ApiData[index] = data[index]
        result = self.apiWriteData(handle, ApiData)
        return(result)

